<?php

/**
 * Implements hook_form_FORM_ID_alter() for install_configure_form().
 *
 * Allows the profile to alter the site configuration form.
 */
function open_badging_form_install_configure_form_alter(&$form, $form_state) {
  // Pre-populate the site name with the server name.
  $form['site_information']['site_name']['#default_value'] = $_SERVER['SERVER_NAME'];
}

/**
 * Implements hook_install_tasks().
 *
 */
function open_badging_install_tasks() {

  return array(
    'open_badging_message_homepage' => array(
      'display_name' => t('Homepage welcome text'),
      'display' => TRUE,
      'type' => 'form',
      'function' => 'open_badging_welcome_text_form'
    ),
  );
}

/**
 * Configuration form to set welcome text for site homepage.
 */
function open_badging_welcome_text_form() {
  $form['open_badging_welcome_explanation'] = array(
    '#markup' => '<h2>' . t('Homepage welcome text') . '</h2>' . t("Below, enter text that will be shown on your site homeage."),
    '#weight' => -1,
  );
  $form['open_badging_welcome_title'] = array(
    '#type' => 'textfield',
    '#title' => t('Welcome headline'),
    '#description' => t('A short description of the community that visitors can understand at a glance.'),
    '#required' => TRUE,
  );

  $form['open_badging_welcome_body'] = array(
    '#type' => 'textarea',
    '#title' => t('Welcome body text'),
    '#description' => t('Enter a couple of sentences elborating about your community.'),
    '#required' => TRUE,
  );
  $form['open_badging_welcome_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and continue')
  );

  return $form;
}

/**
 * Save the configuration form for set welcome text for anonymous users
 * @see open_badging_anonymous_welcome_text_form()
 */
function open_badging_welcome_text_form_submit($form_id, &$form_state) {
  $node = new stdClass();
  $node->title = $form_state['values']['open_badging_welcome_title'];
  $node->uid = 1;
  $node->status = 1;
  $node->created = time();
  $node->type = 'page';
  $node->language = LANGUAGE_NONE;
  $node->body[LANGUAGE_NONE][0]['value'] = $form_state['values']['open_badging_welcome_body'];
  $node->body[LANGUAGE_NONE][0]['format'] = filter_default_format();
  node_save($node);
  variable_set('site_frontpage', 'node/' . $node->nid);

// Rebuild prior to starting menu creation to capture menu items built by other modules, like the contact module.
  menu_rebuild();
  $menu_links['main-menu:all-badges'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'all-badges',
    'router_path' => 'all-badges',
    'link_title' => 'All Badges',
    'options' => array(
      'attributes' => array(
        'title' => 'All badges listing.',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-49',
  );
  // Exported menu link: main-menu:all-earners
  $menu_links['main-menu:all-earners'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'all-earners',
    'router_path' => 'all-earners',
    'link_title' => 'All Earners',
    'options' => array(
      'attributes' => array(
        'title' => 'All earners Listing',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-48',
  );
  // Exported menu link: main-menu:node/1
  $menu_links['main-menu:node/' . $node->nid] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'node/' . $node->nid,
    'router_path' => 'node/%',
    'link_title' => 'Home',
    'options' => array(
      'attributes' => array(
        'title' => $node->title,
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-50',
  );
  // Exported menu link: main-menu:openbadging/add/badge
  $menu_links['main-menu:openbadging/add/badges'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'openbadging/add/badge',
    'router_path' => 'openbadging/add',
    'link_title' => 'Add Badge',
    'options' => array(
      'attributes' => array(
        'title' => 'Add new badge in system.',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  // Exported menu link: main-menu:openbadging/add/issuer_information
  $menu_links['main-menu:openbadging/add/issuer_information'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'openbadging/add/issuer_information',
    'router_path' => 'openbadging/add/issuer_information',
    'link_title' => 'Add Issuer',
    'options' => array(
      'attributes' => array(
        'title' => 'Add new issuer information in system.',
      ),
    ),
    'module' => 'menu',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '0',
  );
  foreach ($menu_links as $menu) {
    menu_link_save($menu);
  }
  //menu_save($menu_links);
  // Clear the menu cache to get rid of any funkiness.
  menu_cache_clear_all();
}